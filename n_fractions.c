//WAP to find the sum of n fractions.
#include<stdio.h>

struct fraction
{
  int num;
  int den;
};

struct fraction input()
{
  struct fraction a;
  printf("Enter the values for numerator and denominator:\n");
  scanf("%d %d",&a.num, &a.den);
  return a;
}

void input_n(int n, struct fraction arr[100])
{  
  for(int i=0;  i<n;  i++)
  {
    arr[i] = input();
  }
}

int gcd(int a, int b)
{
  int i=1,gcd=1;
  for(i=2; i<=a && i<=b;i++)
  {
      if(a%i==0 && b%i==0)
      gcd=i;
   }
  return gcd;
}



struct fraction compute(struct fraction a, struct fraction b)
{  
  struct fraction sum;
  sum.num = (a.num * b.den) + (a.den * b.num);
  sum.den = (a.den * b.den);
  int Gcd = gcd(sum.num, sum.den);
  sum.num = (sum.num/Gcd);
  sum.den = (sum.den/Gcd);
  return sum;
}

struct fraction compute_n(int n, struct fraction arr[100])
{
  struct fraction add;
  add.num = 0;
  add.den = 1;
  for(int i=0; i<n; i++)
  {
    add = compute(add,arr[i]);
  }
  return add;
}

void display(int n, struct fraction arr[100], struct fraction sum)
{
  for(int i=0; i<n; i++)
  {
    printf("%d/%d",arr[i].num,arr[i].den);
    if(i<n-1)
    printf(" + ");
  }  
  printf("= %d/%d",sum.num,sum.den);
}

int main()
{
  int n;
  struct fraction arr[100],sum;
  printf("How many fractions do you want to add?\n");
  scanf("%d",&n);
  input_n(n,arr);
  sum = compute_n(n,arr);
  display(n,arr,sum);
  return 0;
}
